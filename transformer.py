#!/usr/bin/env python
# coding: utf-8

# In[4]:


import csv
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD


EX = Namespace("http://example.org/")
SAREF = Namespace("https://w3id.org/saref#")

g = Graph()

# Read the CSV file
csv_file = r'D:\Folder\4. Data Triplification\household_data_60min_singleindex.csv'
with open(csv_file, 'r') as file:
    reader = csv.DictReader(file)
    headers = reader.fieldnames  # Get the header row
    row_count = 0

    # locations and devices
    locations = {"industrial1": [], "residential1": [], "residential2": [], "residential3": [],
                 "residential4": [], "residential5": [], "residential6": [], "industrial2": [],
                 "industrial3": [], "public1": [], "public2": []}

    
    for column in headers:
        if column.startswith("DE_KN"): 
           
            for location, device_list in locations.items():
                if location in column:
                    device_list.append(column)

    # timestamp
    timestamp_measurements = {}

    # extract measurement values and timestamps
    for row in reader:
        if row_count >= 10:
            break  # Here I only read 10 rows due to performance issue
        timestamp = row['utc_timestamp']  # Timestamp is in the 'utc_timestamp'
        timestamp_measurements[timestamp] = {}

 
        for column in headers:
            if column.startswith("DE_KN"): 
                device_uri = URIRef(f"{EX}device/{column}")

                # Measurement value
                measurement_value_str = row[column].strip()  
                if measurement_value_str:  # Check if the cell is not empty
                    try:
                        measurement_value = float(measurement_value_str)
                    except ValueError:
                        continue  # Skip if not a valid float

                    # Store measurement value mappe to the current timestamp
                    timestamp_measurements[timestamp][device_uri] = measurement_value

        row_count += 1

    # Add triples
    for location, devices in locations.items():
        location_uri = URIRef(f"{EX}location/{location}")
        location_type = "industrial" if "industrial" in location else "residential" if "residential" in location else "public"
        location_type_literal = Literal(location_type, datatype=XSD.string)
        g.add((location_uri, RDF.type, SAREF.Location))
        g.add((location_uri, SAREF.hasLocationType, location_type_literal))

        for device in devices:
            device_uri = URIRef(f"{EX}device/{device}")
            g.add((device_uri, RDF.type, SAREF.Device))
            g.add((device_uri, SAREF.isLocatedIn, location_uri))

            # Add measurement values for each timestamp
            for timestamp, measurements in timestamp_measurements.items():
                if device_uri in measurements:
                    measurement_value = measurements[device_uri]
                    timestamp_uri = URIRef(f"{EX}timestamp/{timestamp}")
                    g.add((device_uri, SAREF.makesMeasurement, URIRef(f"{EX}measurement/{timestamp}")))
                    g.add((URIRef(f"{EX}measurement/{timestamp}"), RDF.type, SAREF.Measurement))
                    g.add((URIRef(f"{EX}measurement/{timestamp}"), SAREF.hasValue, Literal(measurement_value, datatype=XSD.float)))
                    g.add((URIRef(f"{EX}measurement/{timestamp}"), SAREF.hasTimestamp, timestamp_uri))

# Serialize the RDF graph
output_file = r'D:\Folder\4. Data Triplification\transformer4.ttl'
g.serialize(destination=output_file, format='turtle')

print(f"RDF graph has been successfully created and saved to {output_file}.")


# In[ ]:




